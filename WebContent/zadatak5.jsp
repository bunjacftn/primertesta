<%@page import="modul2test.maraton.model.Ucesnik"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	List<Ucesnik> ucesnici = (List<Ucesnik>) request.getAttribute("ucesniciUslov");
%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Učesnici</title>
</head>


<body>
	<h2>Pregled učesnika za uslov</h2>

	<table>
		<tr>
			<th>Ime i prezime</th>
			<th>Kilometri</th>
			<th>Vreme (minuti)</th>
			<th>Tip učesnika</th>
			<th>Završio trku</th>
			<th>Napomena</th>
			<th>Tip učešća</th>
		</tr>
		<%
			for (Ucesnik u : ucesnici) {
		%>
		<tr>
			<td><%=u.getImePrezime()%></td>
			<td><%=u.getBrojPredjenihKilometara()%></td>
			<td><%=u.getVreme()%></td>
			<td><%=u.getTipUcesnika()%></td>
			<td><%=u.getZavrsioTrku()%></td>
			<td><%=u.getNapomena()%></td>
			<td><%=u.getTipUcesca().getNaziv()%></td>
		</tr>
		<%
			}
		%>
	</table>

</body>

</html>