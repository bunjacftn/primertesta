<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="modul2test.maraton.model.TipUcesca"%>
<%@page import="modul2test.maraton.model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	User prijavljeniKorisnik = (User) session.getAttribute("prijavljeniKorisnik");
	List<TipUcesca> tipoviUcesca = (List<TipUcesca>) request.getAttribute("tipoviUcesca");
%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Učesnici</title>
</head>


<body>
	<form method="post" action="DodajUcesnikaServlet2">
		<table>
			<tr>
				<td>Ime i prezime:</td>
				<td><input type="text" name="ime_i_prezime" /></td>
			</tr>
			<tr>
				<td>Broj pređenih kilometara:</td>
				<td><input type="text" name="kilometraza"></td>
			</tr>
			<tr>
				<td>Vreme u minutima:</td>
				<td><input type="text" name="minuti"></td>
			</tr>
			<tr>
				<td>Tip učesnika:</td>
				<td><input type="radio" name="tip_ucesnika" value="Rekreativac" checked>Rekreativac
					<input type="radio" name="tip_ucesnika" value="Profesionalac">Profesionalac</td>
			</tr>
			<tr>
				<td>Završio trku:</td>
				<td><input type="checkbox" name="zavrsio" value="DA"></td>
			</tr>
			<tr>
				<td>Napomena:</td>
				<td><textarea rows="3" cols="30" name="napomena"></textarea></td>
			</tr>
			<tr>
				<td>Tip učešća:</td>
				<td><select name="tip_ucesca">
						<%
							for (TipUcesca tu : tipoviUcesca) {
						%>
						<option value="<%=tu.getId()%>"><%=tu.getNaziv()%></option>
						<%
							}
						%>
					</select></td>
			</tr>
			<tr>
				<td colspan="2" align="right"><input type="submit" value="Dodaj" /></td>
			</tr>
		</table>
	</form>
	<br />
	<%
		if (prijavljeniKorisnik == null) {
	%>
	<a href="zadatak4Prijava.html">Prijava</a>
	<br />
	<%
		} else {
	%>
	<br />
	<a href="PrikaziUcesnikeServlet2">Prikazi ucesnike</a>
	<br />
	<a href="OdjavaServlet">Odjava</a>
	<%
		}
	%>
</body>

</html>