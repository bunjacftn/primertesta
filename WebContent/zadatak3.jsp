<%@page import="java.util.Map"%>
<%@page import="modul2test.maraton.model.Ucesnik"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	@SuppressWarnings("unchecked")
	Map<Integer, Ucesnik> ucesnici = (Map<Integer, Ucesnik>) application.getAttribute("ucesnici");
%> <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Učesnici</title>
</head>
<body>
	<h2>Pregled učesnika</h2>

	<table>
		<tr>
			<th>Ime i prezime</th>
			<th>Kilometri</th>
			<th>Vreme (minuti)</th>
			<th>Tip učesnika</th>
			<th>Završio trku</th>
			<th>Napomena</th>
			<th>Tip učešća</th>
		</tr>
		<%
			for (Ucesnik u : ucesnici.values()) {
		%>
		<tr>
			<td><%=u.getImePrezime()%></td>
			<td><%=u.getBrojPredjenihKilometara()%></td>
			<td><%=u.getVreme()%></td>
			<td><%=u.getTipUcesnika()%></td>
			<td><%=u.getZavrsioTrku()%></td>
			<td><%=u.getNapomena()%></td>
			<td><%=u.getTipUcesca().getNaziv()%></td>
		</tr>
		<%
			}
		%>
	</table>
	<br />
	<a href="zadatak2.jsp">Povratak na unos</a>

</body>

</html>