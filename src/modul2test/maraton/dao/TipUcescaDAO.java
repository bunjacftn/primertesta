package modul2test.maraton.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modul2test.maraton.model.TipUcesca;

public class TipUcescaDAO {

	public static TipUcesca getTipUcescaZaIdIzBaze(int id) throws Exception {
		TipUcesca tipUcesca = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "select naziv from tip_ucesca where id = ?";
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs.next()) {
				String naziv = rs.getString("naziv");
				tipUcesca = new TipUcesca(id, naziv);
			}
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return tipUcesca;
	}

	public static List<TipUcesca> gettAll() throws Exception {
		List<TipUcesca> tipoviUcesca = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "select * from tip_ucesca";
			stmt = ConnectionManager.getConnection().createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				String naziv = rs.getString("naziv");
				TipUcesca tipUcesca = new TipUcesca(id, naziv);
				tipoviUcesca.add(tipUcesca);
			}
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return tipoviUcesca;
	}

}