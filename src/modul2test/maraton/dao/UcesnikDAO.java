package modul2test.maraton.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modul2test.maraton.model.TipUcesca;
import modul2test.maraton.model.Ucesnik;

public class UcesnikDAO {

	public static List<Ucesnik> vratiUcesnikeZaUslovKilometraze(int n) throws Exception {
		List<Ucesnik> ucesnici = new ArrayList<>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "select * from ucesnik where broj_predjenih_kilometara >= ?";
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			stmt.setInt(1, n);
			rs = stmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String imePrezime = rs.getString("ime_i_prezime");
				int brojPredjenihKilometara = rs.getInt("broj_predjenih_kilometara");
				int vreme = rs.getInt("vreme_u_minutima");
				String tipUcesnika = rs.getString("tip_ucesnika");
				String zavrsioTrku = rs.getString("zavrsio_trku");
				String napomena = rs.getString("napomena");
				TipUcesca tipUcesca = TipUcescaDAO.getTipUcescaZaIdIzBaze(rs.getInt("tip_ucesca"));
				ucesnici.add(new Ucesnik(id, imePrezime, brojPredjenihKilometara, vreme, tipUcesnika, zavrsioTrku,
						napomena, tipUcesca));
			}
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return ucesnici;
	}

	public static List<Ucesnik> getAll() throws Exception {
		List<Ucesnik> ucesnici = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "select * from ucesnik";
			stmt = ConnectionManager.getConnection().createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				String imePrezime = rs.getString("ime_i_prezime");
				int brojPredjenihKilometara = rs.getInt("broj_predjenih_kilometara");
				int vreme = rs.getInt("vreme_u_minutima");
				String tipUcesnika = rs.getString("tip_ucesnika");
				String zavrsioTrku = rs.getString("zavrsio_trku");
				String napomena = rs.getString("napomena");
				TipUcesca tipUcesca = TipUcescaDAO.getTipUcescaZaIdIzBaze(rs.getInt("tip_ucesca"));
				Ucesnik u = new Ucesnik(id, imePrezime, brojPredjenihKilometara, vreme, tipUcesnika, zavrsioTrku,
						napomena, tipUcesca);
				ucesnici.add(u);
			}
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return ucesnici;
	}

	public static boolean add(Ucesnik ucesnik) throws Exception {
		PreparedStatement stmt = null;
		try {
			String sql = "insert into "
					+ "ucesnik(ime_i_prezime, broj_predjenih_kilometara, vreme_u_minutima, tip_ucesnika, zavrsio_trku, napomena, tip_ucesca) "
					+ "values(?, ?, ?, ?, ?, ?, ?)";
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, ucesnik.getImePrezime());
			stmt.setInt(i++, ucesnik.getBrojPredjenihKilometara());
			stmt.setInt(i++, ucesnik.getVreme());
			stmt.setString(i++, ucesnik.getTipUcesnika());
			stmt.setString(i++, ucesnik.getZavrsioTrku());
			stmt.setString(i++, ucesnik.getNapomena());
			stmt.setInt(i++, ucesnik.getTipUcesca().getId());
			return 1 == stmt.executeUpdate();
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
