package modul2test.maraton.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import modul2test.maraton.model.User;

public class UserDAO {

	public static User getUserByUsernameAndPassword(String username, String password) throws Exception {
		User user = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "select id from user where username = ? and password = ?";
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			stmt.setString(1, username);
			stmt.setString(2, password);
			rs = stmt.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("id");
				user = new User(id, username, null);
			}
		} finally {
			try {
				stmt.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			try {
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return user;
	}

}
