package modul2test.maraton;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modul2test.maraton.dao.TipUcescaDAO;
import modul2test.maraton.model.TipUcesca;

/**
 * Servlet implementation class Zadatak4PocetnaServlet
 */
public class Zadatak4PocetnaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			List<TipUcesca> tipoviUcesca = TipUcescaDAO.gettAll();
			request.setAttribute("tipoviUcesca", tipoviUcesca);
			request.getRequestDispatcher("zadatak4Forma.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
