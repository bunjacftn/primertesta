package modul2test.maraton.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import modul2test.maraton.dao.ConnectionManager;
import modul2test.maraton.model.TipUcesca;
import modul2test.maraton.model.Ucesnik;

/**
 * Application Lifecycle Listener implementation class InitListener
 *
 */
public class InitListener implements ServletContextListener {

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			ConnectionManager.close();
			System.out.println("zatvorena konekcija");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			initTipUcesca(arg0);
			initUcesnika(arg0);
			ConnectionManager.open();
			System.out.println("otvorena konekcija");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initUcesnika(ServletContextEvent arg0) {
		Map<Integer, Ucesnik> ucesnici = new HashMap<>();
		arg0.getServletContext().setAttribute("ucesnici", ucesnici);
	}

	private void initTipUcesca(ServletContextEvent arg0) {
		Map<Integer, TipUcesca> tipoviUcesca = new HashMap<>();
		tipoviUcesca.put(1, new TipUcesca(1, "trka zadovljstva 5km"));
		tipoviUcesca.put(2, new TipUcesca(2, "polumaraton 21km"));
		tipoviUcesca.put(3, new TipUcesca(3, "maraton 42km"));
		tipoviUcesca.put(4, new TipUcesca(4, "ultramaraton 100km"));
		arg0.getServletContext().setAttribute("tipoviUcesca", tipoviUcesca);
	}

}
