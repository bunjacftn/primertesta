package modul2test.maraton.Ucesnik;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import modul2test.maraton.dao.TipUcescaDAO;
import modul2test.maraton.dao.UcesnikDAO;
import modul2test.maraton.model.TipUcesca;
import modul2test.maraton.model.Ucesnik;

/**
 * Servlet implementation class AjaxDodajServlet
 */
public class AjaxDodajServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			// ako je neka od vrednost null bacice gresku te ce se catchu redirektovati na
			// formu
			String imePrezime = request.getParameter("ime_i_prezime");
			if (imePrezime.equals("")) {
				odrican(response);
				return;
			}

			int kilometraza = Integer.parseInt(request.getParameter("kilometraza"));
			if (kilometraza < 0) {
				odrican(response);
				return;
			}

			int vreme = Integer.parseInt(request.getParameter("minuti"));
			if (vreme < 0) {
				odrican(response);
				return;
			}

			String tipUcesnika = request.getParameter("tip_ucesnika");
			if (tipUcesnika == null) {
				odrican(response);
				return;
			}

			String zavrsioTrku = request.getParameter("zavrsio");
			if (zavrsioTrku == null) {
				zavrsioTrku = "NE";
			}

			String napomena = request.getParameter("napomena");

			int tipUcescaId = Integer.parseInt(request.getParameter("tip_ucesca"));
			TipUcesca tipUcesca = TipUcescaDAO.getTipUcescaZaIdIzBaze(tipUcescaId);

			Ucesnik ucesnik = new Ucesnik(0, imePrezime, kilometraza, vreme, tipUcesnika, zavrsioTrku, napomena,
					tipUcesca);
			UcesnikDAO.add(ucesnik);
			potvrdan(response);

		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
	}

	private static void odrican(HttpServletResponse response) throws Exception {
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "neuspeh");

		// konverzija odgovora u JSON format
		String jsonOdgovor = new ObjectMapper().writeValueAsString(odgovor);
		System.out.println(jsonOdgovor);
		// slanje odgovora
		response.setContentType("application/json; charset=UTF-8");
		response.getWriter().write(jsonOdgovor);
	}

	private static void potvrdan(HttpServletResponse response) throws Exception {
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "uspeh");

		// konverzija odgovora u JSON format
		String jsonOdgovor = new ObjectMapper().writeValueAsString(odgovor);
		System.out.println(jsonOdgovor);
		// slanje odgovora
		response.setContentType("application/json; charset=UTF-8");
		response.getWriter().write(jsonOdgovor);
	}
}
