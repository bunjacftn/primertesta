package modul2test.maraton.Ucesnik;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modul2test.maraton.dao.TipUcescaDAO;
import modul2test.maraton.dao.UcesnikDAO;
import modul2test.maraton.model.TipUcesca;
import modul2test.maraton.model.Ucesnik;
import modul2test.maraton.model.User;

/**
 * Servlet implementation class DodajUcesnikaServlet2
 */
public class DodajUcesnikaServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doPost(request, response);
		response.sendRedirect("Zadatak4PocetnaServlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String redirectStringOnFail = "Zadatak4PocetnaServlet";
		try {
			// ako je neka od vrednost null bacice gresku te ce se catchu redirektovati na
			// formu
			String imePrezime = request.getParameter("ime_i_prezime");
			if (imePrezime.equals("")) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			int kilometraza = Integer.parseInt(request.getParameter("kilometraza"));
			if (kilometraza < 0) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			int vreme = Integer.parseInt(request.getParameter("minuti"));
			if (vreme < 0) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			String tipUcesnika = request.getParameter("tip_ucesnika");
			if (tipUcesnika == null) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			String zavrsioTrku = request.getParameter("zavrsio");
			if (zavrsioTrku == null) {
				zavrsioTrku = "NE";
			}

			String napomena = request.getParameter("napomena");

			int tipUcescaId = Integer.parseInt(request.getParameter("tip_ucesca"));
			TipUcesca tipUcesca = TipUcescaDAO.getTipUcescaZaIdIzBaze(tipUcescaId);

			Ucesnik ucesnik = new Ucesnik(0, imePrezime, kilometraza, vreme, tipUcesnika, zavrsioTrku, napomena,
					tipUcesca);
			UcesnikDAO.add(ucesnik);

			// TODO System.out.println("Ukupan broj ucesnika: " + UcesnikDAO.brojUcesnika());

			// provera ako je user prijavljen moze da vidi ucesnike, ako nije ide na unos
			User user = (User) request.getSession().getAttribute("prijavljeniKorisnik");
			if (user != null) {
				response.sendRedirect("PrikaziUcesnikeServlet2");
				return;
			} else {
				response.sendRedirect(redirectStringOnFail);
				return;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			response.sendRedirect(redirectStringOnFail);
			return;
		}
	}

}
