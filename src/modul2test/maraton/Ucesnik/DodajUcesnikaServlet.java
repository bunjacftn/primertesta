package modul2test.maraton.Ucesnik;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modul2test.maraton.model.TipUcesca;
import modul2test.maraton.model.Ucesnik;

/**
 * Servlet implementation class DodajUcesnikaServlet
 */
public class DodajUcesnikaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doPost(request, response);
		response.sendRedirect("zadatak2.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final String redirectStringOnFail = "zadatak2.jsp";
		try {
			// ako je neka od vrednost null bacice gresku te ce se catchu redirektovati na
			// formu
			String imePrezime = request.getParameter("ime_i_prezime");
			if (imePrezime.equals("")) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			int kilometraza = Integer.parseInt(request.getParameter("kilometraza"));
			if (kilometraza < 0) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			int vreme = Integer.parseInt(request.getParameter("minuti"));
			if (vreme < 0) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			String tipUcesnika = request.getParameter("tip_ucesnika");
			if (tipUcesnika == null) {
				response.sendRedirect(redirectStringOnFail);
				return;
			}

			String zavrsioTrku = request.getParameter("zavrsio");
			if (zavrsioTrku == null) {
				zavrsioTrku = "NE";
			}

			String napomena = request.getParameter("napomena");

			int tipUcescaId = Integer.parseInt(request.getParameter("tip_ucesca"));
			Map<Integer, TipUcesca> tipoviUcesca = (Map<Integer, TipUcesca>) request.getSession().getServletContext()
					.getAttribute("tipoviUcesca");
			TipUcesca tipUcesca = tipoviUcesca.get(tipUcescaId);

			Ucesnik ucesnik = new Ucesnik(0, imePrezime, kilometraza, vreme, tipUcesnika, zavrsioTrku, napomena,
					tipUcesca);

			Map<Integer, Ucesnik> ucesnici = (Map<Integer, Ucesnik>) request.getSession().getServletContext()
					.getAttribute("ucesnici");
			ucesnici.put(ucesnik.getId(), ucesnik);

			System.out.println("Ukupan broj ucesnika: " + ucesnici.size());

			response.sendRedirect("zadatak3.jsp");
		} catch (Exception ex) {
			ex.printStackTrace();
			response.sendRedirect(redirectStringOnFail);
			return;
		}
	}

}
