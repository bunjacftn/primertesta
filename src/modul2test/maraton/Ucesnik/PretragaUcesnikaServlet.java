package modul2test.maraton.Ucesnik;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modul2test.maraton.dao.UcesnikDAO;
import modul2test.maraton.model.Ucesnik;

/**
 * Servlet implementation class PretragaUcesnikaServlet
 */
public class PretragaUcesnikaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int n = Integer.parseInt(request.getParameter("uslov"));
			List<Ucesnik> ucesniciUslov = UcesnikDAO.vratiUcesnikeZaUslovKilometraze(n);

			request.setAttribute("ucesniciUslov", ucesniciUslov);
			request.getRequestDispatcher("zadatak5.jsp").forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
