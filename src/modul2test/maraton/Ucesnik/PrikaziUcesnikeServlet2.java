package modul2test.maraton.Ucesnik;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modul2test.maraton.dao.UcesnikDAO;
import modul2test.maraton.model.Ucesnik;
import modul2test.maraton.model.User;

/**
 * Servlet implementation class PrikaziUcesnikeServlet2
 */
public class PrikaziUcesnikeServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			User user = (User) request.getSession().getAttribute("prijavljeniKorisnik");
			if (user == null) {
				response.sendRedirect("PrijavaServlet");
				return;
			}
			List<Ucesnik> ucesinci = UcesnikDAO.getAll();
			request.setAttribute("ucesnici", ucesinci);
			request.getRequestDispatcher("zadatak4Prikaz.jsp").forward(request, response);
			return;
		} catch (Exception ex) {
			ex.printStackTrace();
			response.sendRedirect("Zadatak4PocetnaServlet");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
