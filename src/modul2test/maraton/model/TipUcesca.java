package modul2test.maraton.model;

public class TipUcesca {
	private int id;
	private String naziv;

	public TipUcesca(int id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipUcesca other = (TipUcesca) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

}
