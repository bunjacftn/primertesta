package modul2test.maraton.model;

public class Ucesnik {
	private int id;
	private String imePrezime;
	private int brojPredjenihKilometara;
	private int vreme;
	private String tipUcesnika;
	private String zavrsioTrku;
	private String napomena;
	private TipUcesca tipUcesca;

	private static int lastId = 0;

	public Ucesnik(int id, String imePrezime, int brojPredjenihKilometara, int vreme, String tipUcesnika,
			String zavrsioTrku, String napomena, TipUcesca tipUcesca) {
		super();
		if (id == 0) {
			this.id = ++lastId;
		} else {
			this.id = id;
		}
		this.imePrezime = imePrezime;
		this.brojPredjenihKilometara = brojPredjenihKilometara;
		this.vreme = vreme;
		this.tipUcesnika = tipUcesnika;
		this.zavrsioTrku = zavrsioTrku;
		this.napomena = napomena;
		this.tipUcesca = tipUcesca;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ucesnik other = (Ucesnik) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public int getBrojPredjenihKilometara() {
		return brojPredjenihKilometara;
	}

	public void setBrojPredjenihKilometara(int brojPredjenihKilometara) {
		this.brojPredjenihKilometara = brojPredjenihKilometara;
	}

	public int getVreme() {
		return vreme;
	}

	public void setVreme(int vreme) {
		this.vreme = vreme;
	}

	public String getTipUcesnika() {
		return tipUcesnika;
	}

	public void setTipUcesnika(String tipUcesnika) {
		this.tipUcesnika = tipUcesnika;
	}

	public String getZavrsioTrku() {
		return zavrsioTrku;
	}

	public void setZavrsioTrku(String zavrsioTrku) {
		this.zavrsioTrku = zavrsioTrku;
	}

	public String getNapomena() {
		return napomena;
	}

	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}

	public TipUcesca getTipUcesca() {
		return tipUcesca;
	}

	public void setTipUcesca(TipUcesca tipUcesca) {
		this.tipUcesca = tipUcesca;
	}

}
