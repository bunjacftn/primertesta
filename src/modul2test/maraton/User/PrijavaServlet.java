package modul2test.maraton.User;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modul2test.maraton.dao.UserDAO;
import modul2test.maraton.model.User;

/**
 * Servlet implementation class PrijavaServlet
 */
public class PrijavaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("prijavljeniKorisnik");
		if (user != null) {
			response.sendRedirect("Zadatak4PocetnaServlet");
			return;
		}

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			// čitanje korisnika iz baze
			user = UserDAO.getUserByUsernameAndPassword(username, password);
			if (user == null) {
				// neuspešna prijava
				response.sendRedirect("zadatak4Prijava.html");
				return;
			}

			// pamćenje korisnika u sesiji
			HttpSession session = request.getSession();
			session.setAttribute("prijavljeniKorisnik", user);

			// uspešna prijava
			response.sendRedirect("Zadatak4PocetnaServlet");
			return;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
